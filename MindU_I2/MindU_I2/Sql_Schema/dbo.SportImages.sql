﻿CREATE TABLE [dbo].[SportImages] (
    [imgId]   INT            NOT NULL,
    [imgPath] NVARCHAR (MAX) NOT NULL,
    [sid]     INT            NOT NULL,
    PRIMARY KEY CLUSTERED ([imgId] ASC),
    CONSTRAINT [FK_SportImages_ToSports] FOREIGN KEY ([sid]) REFERENCES [dbo].[Sports] ([sid])
);

