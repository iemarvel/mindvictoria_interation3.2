﻿CREATE TABLE [dbo].[ArtRating] (
    [rid]      INT           NOT NULL,
    [likes]    INT NOT NULL,
    [aid]      INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([rid] ASC),
    CONSTRAINT [FK_ArtRating_ToArts] FOREIGN KEY ([aid]) REFERENCES [dbo].[Arts] ([aid])
);

