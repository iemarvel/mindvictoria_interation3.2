﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MindU_I2.Models;

namespace MindU_I2.Controllers
{
    public class SportsController : Controller
    {
        private MindUEntities db = new MindUEntities();


        // GET: Sports
        public ActionResult Index(string name, string category, int page = 0)
        {
            const int pageSize = 10;
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Please choose category", Value = "Please choose category" });
            items.Add(new SelectListItem { Text = "Yoga", Value = "Yoga" });
            items.Add(new SelectListItem { Text = "Pilates", Value = "Pilates" });
            items.Add(new SelectListItem { Text = "Tai Chi", Value = "Tai Chi" });
            items.Add(new SelectListItem { Text = "Fitness Center", Value = "Fitness Center" });
            items.Add(new SelectListItem { Text = "Swimming Complex", Value = "Swimming Complex" });
            ViewBag.defaultSelect = "Please choose category";
            ViewBag.selectList = items;
            if (name != null && category != "Please choose category")
            {
                var count = db.Sports.Where(x => x.name.Contains(name) && x.category.Equals(category)).ToList().Count();
                var data = db.Sports.Where(x => x.name.Contains(name) && x.category.Equals(category)).OrderBy(s => s.sid).Skip(page * pageSize).Take(pageSize).ToList();
                this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
                this.ViewBag.Page = page;
                return View(data);
            }
            else
                if (name == null && category != "Please choose category" && category != null)
            {
                var count = db.Sports.Where(x => x.category.Equals(category)).ToList().Count();
                var data = db.Sports.Where(x => x.category.Equals(category)).OrderBy(s => s.sid).Skip(page * pageSize).Take(pageSize).ToList();
                this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
                this.ViewBag.Page = page;
                return View(data);
            }
            else
                if (name != null && category == "Please choose category")
            {
                var count = db.Sports.Where(x => x.name.Contains(name)).ToList().Count();
                var data = db.Sports.Where(x => x.name.Contains(name)).OrderBy(s => s.sid).Skip(page * pageSize).Take(pageSize).ToList();
                this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
                this.ViewBag.Page = page;
                return View(data);
            }
            else
            {
                var count = db.Sports.ToList().Count();
                var data = db.Sports.OrderBy(s => s.sid).Skip(page * pageSize).Take(pageSize).ToList();
                this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
                this.ViewBag.Page = page;
                return View(data);
            }
        }

        public ActionResult Yoga()
        {
            return View();
        }

        public ActionResult TaiChi()
        {
            return View();
        }

        public ActionResult Swimming()
        {
            return View();
        }

        public ActionResult Pilates()
        {
            return View();
        }

        public ActionResult Fitness()
        {
            return View();
        }

        public string Like(int id)
        {
            SportRating update = db.SportRatings.ToList().Find(u => u.rid == id);
            update.likes += 1;
            db.SaveChanges();
            return update.likes.ToString();
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

