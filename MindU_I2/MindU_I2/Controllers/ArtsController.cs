﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MindU_I2.Models;

namespace MindU_I2.Controllers
{
    public class ArtsController : Controller
    {
        private MindUEntities db = new MindUEntities();

        // GET: Arts
        public ActionResult Index(string name, string category, int page = 0)
        {
            const int pageSize = 10;
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem { Text = "Please choose category", Value = "Please choose category" });
            items.Add(new SelectListItem { Text = "Art Gallery", Value = "Art Gallery" });
            items.Add(new SelectListItem { Text = "Art Museum", Value = "Art Museum" });
            items.Add(new SelectListItem { Text = "Art School", Value = "Art School" });
            ViewBag.defaultSelect = "Please choose category";
            ViewBag.selectList = items;
            if (name != null && category != "Please choose category")
            {
                var count = db.Arts.Where(x => x.name.Contains(name) && x.category.Equals(category)).ToList().Count();
                var data = db.Arts.Where(x => x.name.Contains(name) && x.category.Equals(category)).OrderBy(s => s.aid).Skip(page * pageSize).Take(pageSize).ToList();
                this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
                this.ViewBag.Page = page;
                return View(data);
            }
            else
                if (name == null && category != "Please choose category" && category != null)
            {
                var count = db.Arts.Where(x => x.category.Equals(category)).ToList().Count();
                var data = db.Arts.Where(x => x.category.Equals(category)).OrderBy(s => s.aid).Skip(page * pageSize).Take(pageSize).ToList();
                this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
                this.ViewBag.Page = page;
                return View(data);
            }
            else
                if (name != null && category == "Please choose category")
            {
                var count = db.Arts.Where(x => x.name.Contains(name)).ToList().Count();
                var data = db.Arts.Where(x => x.name.Contains(name)).OrderBy(s => s.aid).Skip(page * pageSize).Take(pageSize).ToList();
                this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
                this.ViewBag.Page = page;
                return View(data);
            }
            else
            {
                var count = db.Arts.ToList().Count();
                var data = db.Arts.OrderBy(s => s.aid).Skip(page * pageSize).Take(pageSize).ToList();
                this.ViewBag.MaxPage = (count / pageSize) - (count % pageSize == 0 ? 1 : 0);
                this.ViewBag.Page = page;
                return View(data);
            }
        }

        // GET: Arts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Art arts = db.Arts.Find(id);
            if (arts == null)
            {
                return HttpNotFound();
            }
            return View(arts);
        }

        public ActionResult ArtSchool()
        {
            return View();
        }

        public ActionResult Museum()
        {
            return View();
        }

        public ActionResult ArtGallery()
        {
            return View();
        }
        public ActionResult ArtCard()
        {
            return View();
        }

        public string Like(int id)
        {
            ArtRating update = db.ArtRatings.ToList().Find(u => u.rid == id);
            update.likes += 1;
            db.SaveChanges();
            return update.likes.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}

