﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MindU_I2.Models
{
    public class Venue
    {
        public string name { get; set; }
        public string formatted_address { get; set; }
        public string rating { get; set; }
        public string place_id { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
        public string formatted_phone_number { get; set; }       
        public string website { get; set;}
    }
}